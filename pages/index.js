import React, {Component} from "react"

import TodoList from "../container/TodoList"

export default class extends Component {
  render() {
    return (
      <React.Fragment>
        <p>Hello, world!</p>
        <TodoList />
      </React.Fragment>
    )
  }
}

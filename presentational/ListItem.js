import React, {Component} from "react"

export default class ListItem extends Component {
  onDelete() {
    this.props.onDelete && this.props.onDelete()
  }

  render() {
    return (
      <React.Fragment>
        <p>{this.props.text}</p>
        <button onClick={() => this.onDelete()}>Delete Item</button>
      </React.Fragment>
    )
  }
}

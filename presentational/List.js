import React, {Component} from "react"

import ListItem from "./ListItem.js"

export default class List extends Component {
  render() {
    return (
      <ul>
        {this.props.items.map((item, index) => (
          <ListItem key={index} text={item} />
        ))}
      </ul>
    )
  }
}

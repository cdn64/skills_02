import React, {Component} from "react"

import List from "../presentational/List"

export default class TodoList extends Component {
  state = {
    newTodo: "",
    todos: []
  }

  changeNewTodo(newTodo) {
    this.setState({newTodo})
  }

  addTodo() {
    this.setState(state => ({
      newTodo: "",
      todos: [state.newTodo, ...state.todos]
    }))
  }

  render() {
    return (
      <React.Fragment>
        <input
          type="text"
          onChange={event => this.changeNewTodo(event.target.value)}
        />
        <button onClick={() => this.addTodo()}>Add Todo</button>
        <List items={this.state.todos} />
      </React.Fragment>
    )
  }
}

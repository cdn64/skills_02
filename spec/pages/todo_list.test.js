import {shallow, mount} from "enzyme"
import React from "react"

import ListItem from "../../presentational/ListItem.js"
import List from "../../presentational/List.js"
import TodoList from "../../container/TodoList.js"

const text = "Hello, world!"
const onDelete = jest.fn()
const listItem = shallow(<ListItem text={text} onDelete={onDelete} />)

const list = shallow(<List items={["text 1"]} />)

const todoList = mount(<TodoList />)

describe("list item", () => {
  it("shows the given text", () => {
    expect(listItem.find("p").text()).toEqual(text)
  })

  it("displays a delete button", () => {
    expect(listItem.find("button").length).toEqual(1)
  })

  it("triggers onDelete when delete button clicked", () => {
    listItem.find("button").simulate("click")
    expect(onDelete.mock.calls.length).toEqual(1)

    const listItem2 = shallow(<ListItem text={text} />)
    listItem2.find("button").simulate("click")
  })
})

describe("list", () => {
  it("displays List Component", () => {
    expect(list.find("ListItem").length).toEqual(1)
  })
})

describe("todo list", () => {
  it("displays a List Component", () => {
    expect(todoList.find("List").length).toEqual(1)
    expect(todoList.find("ListItem").length).toEqual(0)
  })

  it("displays an input field and a button", () => {
    expect(todoList.find("input").length).toEqual(1)
    expect(todoList.find("button").length).toEqual(1)
  })

  it("can create a new ListItem", () => {
    todoList.find("input").simulate("change", {target: {value: "Kaffee holen"}})
    todoList.find("button").simulate("click")
    expect(todoList.find("ListItem").length).toEqual(1)
  })
})
